package challenge3;

public class Main {
   public static void main(String[] args) {

      Hewan hewan = new Anjing();
      Anjing anjing = (Anjing) hewan;
      anjing.eat();

      //klo pake yg dibawah ini error kak
      // Anjing anjing = (Anjing) new Hewan();
      // anjing.eat();
   }
}